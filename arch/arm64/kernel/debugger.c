#include <linux/debugger.h>
#include <linux/fs.h>
#include <linux/init.h>
#include <linux/module.h>
#include <linux/uaccess.h>
MODULE_LICENSE("GPL");
MODULE_AUTHOR("Oleg Sazonov(x0r3d)");

static struct file *f;

static void hooker(void) { printk("Hooked!"); };

static int __init x0r3d_init(void) {
  printk("x0r3d: driver started!\n");
  return 0;
};

static void __exit x0r3d_exit(void) { printk("x0r3d: driver stopped!\n"); };

module_init(x0r3d_init);
module_exit(x0r3d_exit);
